import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var google;

@Component({
  selector: 'home-page',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad(){
    this.loadMap();
  }

  loadMap(){

    let latLng = new google.maps.LatLng(20.656639, -103.326180);

    const builds = [
      {
        "name": 'A',
        "lat": 20.653998,
        "lng": -103.325708
      },
      {
        "name": 'D',
        "lat": 20.654495,
        "lng": -103.325300
      },
      {
        "name": 'E',
        "lat": 20.655635,
        "lng": -103.325558
      },
      {
        "name": 'F',
        "lat": 20.655795,
        "lng": -103.325976
      },
      {
        "name": 'G',
        "lat": 20.655860,
        "lng": -103.327001
      },
      {
        "name": 'H',
        "lat": 20.656036,
        "lng": -103.326448
      },
      {
        "name": 'I',
        "lat": 20.656096,
        "lng": -103.325526
      },
      {
        "name": 'J',
        "lat": 20.656207,
        "lng": -103.326094
      },
      {
        "name": 'K',
        "lat": 20.656377,
        "lng": -103.326046
      },
      {
        "name": 'L',
        "lat": 20.656764,
        "lng": -103.325220
      },
      {
        "name": 'M',
        "lat": 20.656639,
        "lng": -103.326180
      },
      {
        "name": 'N',
        "lat": 20.656940,
        "lng": -103.326201
      },
      {
        "name": 'O',
        "lat": 20.657311,
        "lng": -103.326250
      },
      {
        "name": 'P',
        "lat": 20.657321,
        "lng": -103.325418
      },
      {
        "name": 'Q',
        "lat": 20.657657,
        "lng": -103.324946
      },
      {
        "name": 'R',
        "lat": 20.657632,
        "lng": -103.325654
      },
      {
        "name": 'S',
        "lat": 20.657617,
        "lng": -103.326325
      },
      {
        "name": 'T',
        "lat": 20.657893,
        "lng": -103.325493
      },
      {
        "name": 'U',
        "lat": 20.658109,
        "lng": -103.325515
      },
      {
        "name": 'V',
        "lat": 20.658325,
        "lng": -103.326271
      },
      {
        "name": 'V2',
        "lat": 20.658079,
        "lng": -103.326234
      },
      {
        "name": 'W',
        "lat": 20.658059,
        "lng": -103.326845
      },
      {
        "name": 'X',
        "lat": 20.658280,
        "lng": -103.326942
      },
      {
        "name": 'Z1',
        "lat": 20.657216,
        "lng": -103.327703
      },
      {
        "name": 'Z2',
        "lat": 20.657271,
        "lng": -103.327886
      },
      {
        "name": 'UCT 1',
        "lat": 20.656458,
        "lng":  -103.325225
      },
      {
        "name": 'UCT 2',
        "lat": 20.656227,
        "lng":  -103.325247
      },
      {
        "name": 'CTA',
        "lat": 20.656352,
        "lng": -103.324909
      }
    ]

    let mapOptions = {
      center: latLng,
      zoom: 17,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    for (var build of builds) {
      this.addMarker(build);
    }
    // const build = {
    //   "lat": 20.653998,
    //   "lng": -103.325708
    // }
    // this.addMarker(build);
  }
  addMarker(build){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: {"lat": build.lat, "lng": build.lng},
      label: build.name
    });

    let content = "<h4>Information!</h4>";

    this.addInfoWindow(marker, content);
  }
  addInfoWindow(marker, content){

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });

  }
}
